(ns dmos.raudondvaris.EntryServlet
  (:gen-class :extends javax.servlet.http.HttpServlet)
  (:require
    [ring.util.servlet :refer [defservice]]
    [dmos.raudondvaris.core :refer [app]]))

(defservice app)
